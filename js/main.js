const endpoint = '/.netlify/functions/storepost';
const campaign = 'techfast50au2';
document.querySelector('#submit').addEventListener('click', (e) => {
    e.preventDefault();
    const name = document.querySelector("#name").value;
    const commitment = document.querySelector("#commitment").value;
    const image = 'http://placeimg.com/1200/627/animals';
    storePost(endpoint, campaign, name, commitment, image);
});

const storePost = (endpoint, campaign, name, commitment, image) => {
    console.log(endpoint, campaign, name, commitment, image);
    axios.post(endpoint, {
        campaign: campaign,
        full_name: name,
        description: commitment,
        status: 'active',
        post_image_url: image,
    })
      .then(function (response) {
        console.log(response);
        alert('Commitment submitted!');
      })
      .catch(function (error) {
        console.log(error);
      });
};


