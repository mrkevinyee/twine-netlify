var gulp = require('gulp'); 
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var jsFiles = [
	'src/app.js',
];

gulp.task('default', function () {
    return gulp.watch('src/*.js', gulp.series('scripts'));
});


gulp.task('scripts', function (done) {
    return  gulp.src(jsFiles)
    .pipe(concat('all.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./js/'));
});