var tileController = {

    modal: null,
    rtime: null,
    timeout: false,
    delta: 200,
    tileCount: 0,

    execute: function (tileObject) {

      tileController.init(tileObject);
      tileController.clean(tileObject);

    },

    init: function (tileObject) {

      tileController.initTiles(tileObject);

      tileObject.attr('id', 'tile-' + tileController.tileCount);
    },

    resizeHeight: function (tileObject) {

      var tileComponents = $('#main-container .unlimitedtile');
    
      tileComponents.each(function(index){

        var height = 0;
        var tileComponent = $(this);

        tileComponent.find('.read-more').removeClass('absolute');

        tileComponent.find('.unlimitedtile-box').each(function(index){
          var tile = $(this);
          height = tileController.adjustTileheight(tile, height);
        });

        tileComponent.find(".unlimitedtile-content").css('height', height + 'px');
        tileComponent.find('.read-more').addClass('absolute');

      });
    },

    initTiles: function (tileObject) {

      var height = 0;

      //tileController.tileCount++; // dynamic id if multiple video player on the page
      tileObject.attr('instance', tileController.tileCount);

      tileObject.find('.unlimitedtile-box').each(function(index){

        index = parseInt(index + 1);
        var tile = $(this);

        /*
        if (tileController.imageNotExist(tile, index) &&
            tileController.titleNotExist(tile, index) &&
            tileController.typeNotExist(tile, index) &&
            tileController.urlNotExist(tile, index) &&
            tileController.popupNotExist(tile, index)
        ) {

           tile.parent().remove();
        }
        */
      });

      // var tileLength = tileObject.find('.unlimitedtile-box').length;

      // if (tileLength == 3 || tileLength == 5 || tileLength == 6 || tileLength == 9 || tileLength == 12 || tileLength == 11) {
      //   tileObject.find('.unlimitedtile-box').parent().attr('class', 'col-lg-4 col-md-4 col-sm-6 col-xs-6 tile-box-container');
      // }

      // else if (tileLength == 4 || tileLength == 7 || tileLength == 8 || tileLength == 10) {
      //   tileObject.find('.unlimitedtile-box').parent().attr('class', 'col-lg-3 col-md-3 col-sm-6 col-xs-6 tile-box-container');
      // }

      // else {
      //   // force to hide tiles
      // }

      // setTimeout(function() {

      // });
      
      // check first tile form
      // var tile = tileObject.find('.unlimitedtile-box').eq(0);
      // tileController.hideElements(tile, 1, tileObject);

      // set modal event

      tileObject.find('.unlimitedtile-box').each(function(index){
        var tile = $(this);

        // hide elements for tile type
        // tileController.hideElements(tile, parseInt(index + 1));

        height = tileController.adjustTileheight(tile, height);

        tile.attr('index', index);

        $(tile.find('.unlimitedtile-cta')).click(tileController.tileClickHandler);
      });

      tileObject.find(".unlimitedtile-content").css('height', height + 'px');
      tileObject.find('.read-more').addClass('absolute');

    },

    tileClickHandler: function (e) {

      var tile = $(e.currentTarget).parent();
      var index = parseInt(tile.attr('index')) + 1;

      if (tile.find('.popup-content').text().indexOf("${popup" + index + "}") == -1) {
        e.preventDefault();
        tileController.setModalData(tile, index);

        $('.fr-modal-dialog').removeClass('full-width');
        $('.fr-modal-body').removeClass('container content-container');
        $('.unlimitedtile.branded-component').addClass('preview-modal');
      }

      var title = tile.find('.title').text().trim();

      if(title == "${title" + index + "}") {
        title = '[no-title]';
      }

    },

    hideElements: function (tile, index, tileObject) {
        if (tileController.imageNotExist(tile, index)) {
          tile.find('.unlimitedtile-img').hide();
          tileObject.addClass('hide-imgs');
        }

        if (tileController.titleNotExist(tile, index) &&
            tileController.typeNotExist(tile, index) &&
            tileController.descriptionNotExist(tile, index)
        ) {
          tile.find('.unlimitedtile-content').hide();
          tileObject.addClass('hide-unlimitedtile-content');
        }

        if (tileController.typeNotExist(tile, index)) {
          tile.find('.unlimitedtile-content .type').hide();
          tileObject.addClass('hide-type');
        }

        if (tileController.descriptionNotExist(tile, index)) {
          tile.find('.unlimitedtile-content .description').hide();
          tileObject.addClass('hide-description');
        }
    },

    adjustTileheight: function(tile, height) {

      var tileContent = tile.find(".unlimitedtile-content");
      tileContent.css('height','auto');


        if(tileContent.innerHeight() > height) {

          height = tileContent.innerHeight();
        }

      return height;
    },

    setModalData: function(tile, index) {
      
      var data = {
        title: '',
        popupContent: ''
      };

      data.title = tile.find('.title').html();
      data.popupContent = tile.find('.popup-content').html();

      if(data.title == "${title" + index + "}") {
        data.title = '';
      }
      
      modalController.openModal(data);

    },

    imageNotExist: function(tile, index) {
      console.log(tile.find('.unlimitedtile-img img').attr('src'));
      return (tile.find('.unlimitedtile-img img').attr('src').indexOf("${image" + index + "}") != -1)? true : false;
    },
    
    titleNotExist: function(tile, index) {
      return (tile.find('.title').text().indexOf("${title" + index + "}") != -1)? true: false;
    },

    typeNotExist: function(tile, index) {
      return (tile.find('.type').text().indexOf("${type" + index + "}") != -1)? true: false;
    },

    descriptionNotExist: function(tile, index) {
      return (tile.find('.description').text().indexOf("${description" + index + "}") != -1)? true: false;
    },

    urlNotExist: function(tile, index) {
      return (tile.find('.unlimitedtile-cta').attr('href').indexOf("${url" + index + "}") != -1)? true: false;
    },

    popupNotExist: function(tile, index) {
      return (tile.find('.popup-content').text().indexOf("${popup" + index + "}") != -1)? true: false;
    },


    clean: function (tileObject) {
    },
};