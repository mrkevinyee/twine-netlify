var appController = {
    modal: null,
    rtime: null,
    timeout: !1,
    delta: 200,
    startTileCount: 13,
    tileCount: 0,
    tileType: 'link',
    modalEditorLink: '/tools/modal-editor/index.html',
    previewHTML: '',

    init: function () {
        modalController.init(),
        appController.start()
    },

    start: function () {
        appController.initEvents(),
        appController.addInputs(),
        appController.toggleModalButton(),
        appController.initSortable()
    },

    initEvents: function () {
        document.getElementById("convert").onclick = function (t) {
            t.preventDefault();

            if (appController.requiredInputs()) {
                $("#result").text(appController.outputHTML());
            }
        },

        document.getElementById("import").onclick = function (t) {
            t.preventDefault();
            var html = document.getElementById("import-text-editor").value;
            var tiles = appController.parseText(html);
            appController.addDataToInputs(tiles);
        },

        document.getElementById("add-tile").onclick = function (t) {
            t.preventDefault();
            appController.addTile();
        },

        document.getElementById("delete-tile").onclick = function (t) {
            t.preventDefault();
            appController.deleteTile();
        },

        document.getElementById("modal-checkbox").addEventListener('change', function (t) {
            t.preventDefault();
            appController.tileType = t.target.checked ? 'modal' : 'link';
            appController.toggleModalButton();
            appController.updateTileType();
        }),

        document.getElementById("preview").onclick = function(t) {
            t.preventDefault();

            if (appController.requiredInputs()) {
                $("#convert").click();
                appController.previewTiles();
                tileController.execute($('.unlimitedtile'));
            }
        },
        
        document.getElementById("modal-editor").onclick = function(t) {
            window.open(appController.modalEditorLink);
        },

        document.getElementById("copy-text").onclick = function(t) {
            appController.copyToClipboard($('#result').text());
            alert('HTML copied to clipboard.');
        }
    },

    addInputs: function() {
        for (var currentTile = 1; currentTile <= appController.startTileCount; currentTile++) {
            this.addTile(appController.tileType);
        }
    },

    addTile: function() {
        var type = appController.tileType;
        var count = ++appController.tileCount;

        if (type == 'modal') {
            var input = '<input id="tile' + count + '-modal" name="modal" type="text" placeholder="Modal">';
        } else {
            var input = '<input id="tile' + count + '-link" name="link" type="text" placeholder="Link">';
        }

        var html = '<fieldset id="tile-inputs' + count + '" class="tile-inputs">' +
            '<legend><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>' +
            'Tile ' + count + '</legend>' +
            '<input id="tile' + count + '-image" name="image" type="text" placeholder="Image Link">' +
            '<input id="tile' + count + '-title" name="title" type="text" placeholder="Title">' +
            '<input id="tile' + count + '-desc" name="desc" type="text" placeholder="Description">' +
            input +
            '</fieldset>';

        $('#tile-container').append(html);
    },

    deleteTile: function() {
        if (appController.tileCount > appController.startTileCount) {
            appController.tileCount--;
            $('#tile-container').children().last().remove();
        }
    },

    updateTileType: function() {
        var type = appController.tileType;

        if (type == 'modal') {
            $('[id$=-link]').each(function (index) {
                $(this).replaceWith('<input id="tile' + (index + 1) + '-modal" name="modal" type="text" placeholder="Modal (Modal title pulled from tile title)">');
            });
        } else {
            $('[id$=-modal]').each(function (index) {
                $(this).replaceWith('<input id="tile' + (index + 1) + '-link" name="link" type="text" placeholder="Link">');
            });
        }
    },

    outputHTML: function () {
        appController.previewHTML = '';

        // Hide description if none exists
        if (appController.hasAnyDesc()) {
            var hide = '';
        } else {
            var hide = 'hidden';
        }

        $('fieldset').each(function (index) {
            var inputs = $(this).find('input');
            var image = inputs[0].value;
            var title = inputs[1].value;
            var desc = inputs[2].value;
            var cols = appController.changeTileCols();

            if (appController.tileType == 'link') {
                var link = inputs[3].value;
                var modalContent = '${popup' + (index + 1) + '}';
            } else {
                var link = '${url' + (index + 1) + '}';
                var modalContent = inputs[3].value;
            }

            // Store html for previewing tiles
            appController.previewHTML +=
                '<div class="' + cols + ' unlimitedtile-box-container">' +
                '<div class="unlimitedtile-box">' +
                '<a class="unlimitedtile-cta" href="' + link + '">' +
                '<div class="unlimitedtile-img">' +
                '<img src="' + image + '">' +
                '</div>' +
                '<div class="unlimitedtile-content">' +
                '<h4 class="title">' + title + '</h4>' +
                '<p class="description ' + hide + '">' + desc + '</p>' +
                '<p class="read-more"><i></i></p>' +
                '</div>' +
                '</a>' +
                '<div class="popup-content">' + modalContent + '</div>' +
                '</div>' +
                '</div>\n';
        });

        return appController.previewHTML;
    },

    // Try to make last row not single tile
    changeTileCols: function () {
        var tileCount = appController.tileCount;
        var threeCols = 'col-lg-4 col-md-4 col-sm-6 col-xs-6';
        var fourCols = 'col-lg-3 col-md-3 col-sm-6 col-xs-6';

        if (tileCount % 3 == 0) {
            return threeCols;
        } else if (tileCount % 4 == 0) {
            return fourCols;
        } else if (tileCount % 3 == 2) {
            return threeCols;
        } else if (tileCount % 4 == 2 || tileCount % 4 == 3) {
            return fourCols;
        } else {
            return threeCols;
        }
    },

    parseText: function (text) {
        var html = $($.parseHTML(text));
        var length = html.find('.unlimitedtile-cta').length;
        var tiles = [];

        for (var count = 0; count < length; count++) {
            var data = {
                image: html.find('img').eq(count).attr('src'),
                title: html.find('.title').eq(count).text(),
                description: html.find('.description').eq(count).text(),
                link: html.find('.unlimitedtile-cta').eq(count).attr('href'),
                modalContent: html.find('.popup-content').eq(count).html(),
            }

            tiles.push(data);
        }

        return tiles;
    },

    addDataToInputs: function(tiles) {
        // Ensure we have same amount of tiles in tile editor
        if (tiles.length == 0) { return false }

        while (appController.tileCount != tiles.length) {
            if (appController.tileCount > tiles.length) {
                appController.deleteTile();
            } else {
                appController.addTile();
            }
        }

        var type = 'link';

        if (tiles[0].link.match(/\${url[0-9]+}/)) {
            type = 'modal';

            if (!$('#modal-checkbox').is(":checked")) {
                $('#modal-checkbox').trigger('click');
            }
        } else {
            type = 'link';

            if (!$('#modal-checkbox').is(":not(:checked)")) {
                $('#modal-checkbox').trigger('click');
            }
        }

        tiles.forEach(function (item, index) {
            var tileNum = index + 1;
            $('#tile' + tileNum + '-image').val(item.image);
            $('#tile' + tileNum + '-title').val(item.title);
            $('#tile' + tileNum + '-desc').val(item.description);
            $('#tile' + tileNum + '-link').val(item.link);
            $('#tile' + tileNum + '-modal').val(item.modalContent);
        });
    },

    previewTiles: function () {
        var data = {
            title: '',
            popupContent: appController.previewHTML,
        };

        $('.fr-modal-dialog').addClass('full-width');
        $('.fr-modal-body').addClass('container content-container');
        $('.unlimitedtile.branded-component').removeClass('preview-modal');
        modalController.openModal(data);
    },

    // Currently only description optional
    requiredInputs: function () {
        var canConvert = true;
        $('input[id^=tile]').not('input[id$=-desc]').each(function (index) {
            if ($(this).val() == "") {
                alert('Please fill in all inputs.');
                canConvert = false;
                return false;
            }
        });
        return canConvert;
    },

    hasImage: function () {
        var hasImage = true;
        $('input[id$=-image]').each(function (index) {
            if ($(this).val() == "") {
                hasImage = false;
                return false;
            }
        });
        return hasImage;
    },

    hasTitle: function () {
        var hasTitle = true;
        $('input[id$=-title]').each(function (index) {
            if ($(this).val() == "") {
                hasTitle = false;
                return false;
            }
        });
        return hasTitle;
    },

    hasAnyDesc: function () {
        var hasDesc = false;
        $('input[id$=-desc]').each(function (index) {
            if ($(this).val() != "") {
                hasDesc = true;
                return false;
            }
        });
        return hasDesc;
    },

    toggleModalButton: function () {
        if (appController.tileType == 'link') {
            $('#modal-editor').hide();
        } else {
            $('#modal-editor').show();
        }
    },

    initSortable: function () {
        $("#tile-container").sortable({
            axis: "y",
            cursor: "move",
            update: appController.updateSort
        });
    },

    // Traverse tiles and update sort
    updateSort: function (event, ui) {
        $('#tile-container').children().each(function (index) {
            var count = index + 1;
            $(this).attr('id', 'tile-inputs' + count);

            $(this).children().each(function(index) {
                if ($(this).prop("tagName") == 'LEGEND') {
                    var title = $(this).html().replace(/Tile [0-9]+/, 'Tile ' + count);
                    $(this).html(title);
                } else if ($(this).prop("tagName") == 'INPUT') {
                    var id = $(this).attr('id').replace(/[0-9]+/, count);
                    $(this).attr('id', id);
                }
            });
        });
    },

    copyToClipboard: function (str) {
        var element = document.createElement('textarea');
        element.value = str;
        element.setAttribute('readonly', '');
        element.style.position = 'absolute';
        element.style.left = '-9999px';
        document.body.appendChild(element);

        var selected = document.getSelection().rangeCount > 0
            ? document.getSelection().getRangeAt(0)
            : false;

        element.select();
        document.execCommand('copy');
        document.body.removeChild(element);

        if (selected) {
            document.getSelection().removeAllRanges();
            document.getSelection().addRange(selected);
        }
    },
};

appController.init();