var modalController = {

    modal: null,

    execute: function (tileObject) {

      modalController.init(tileObject);
      modalController.clean(tileObject);

    },

    init: function (tileObject) {

      modalController.initModal(tileObject);

    },

    initModal: function (tileObject) {

      modalController.modal = $('.fr-modal');

      if($('#main-container').hasClass('dark')) {
        modalController.modal.addClass('dark');
      }

      $('.modal-close, .fr-modal-backdrop').click(function(e) {
        e.preventDefault();
        modalController.hideModal();
      });

    },

    openModal: function(data) {
      modalController.modal.find('.modal-heading').html(data.title);
      modalController.modal.find('.fr-modal-body').html(data.popupContent);

      //set modal events
      /*
      modalController.modal.find('.fr-modal-body a').click(function(e){
        var string = $(this).attr('href');

        if(string.indexOf("popup") != -1) {
          e.preventDefault();
          var numbers = string.match(/\d+/g).map(Number);

          modalController.hideModal();
          tileController.setModalData($('#tile-' + numbers[0]  + ' .tile-box-container').eq(parseInt(numbers[1]) - 1).find('.tile-box'));
          console.log('was here');
        }

        if(string.indexOf("scrollto") != -1) {
          e.preventDefault();

          var id = string.replace('scrollto-','');

          modalController.hideModal();
          appController.scrollToSection($('#' + id).offset().top );
        }

      });*/

      modalController.showModal();

    },

    showModal: function() {
      $('body').css('overflow', 'hidden');
      modalController.modal.css('display', 'block');
      modalController.modal.find('.fr-modal-backdrop').css('display', 'block');
    },

    hideModal: function() {
      $('body').css('overflow', 'auto');
      modalController.modal.hide();
      modalController.modal.hide();
    },
};