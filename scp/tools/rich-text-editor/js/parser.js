var parserController = {
    init: function() {

		parserController.start();
    },

    start: function() {

        // var value = parserController.properties.banner.title;

        // if(value != '') {
        //     $('.banner').find('.headline h1').html(value);
        // }

        // value = parserController.properties.banner.description;

        // if(value != '') {
        //     $('.banner').find('.subtitle').html(value);
        // }

        // value = parserController.properties.banner.buttonURL;

        // if(value != '') {
        //     $('.banner').find('.headline-cta a').attr('href', value);
        // }

        // $('.banner').find('.headline-cta a').html(parserController.properties.banner.buttonText);


        for (var i = 0; i < parserController.properties.length; i++) {
            for (var j = 0; j < parserController.properties[i].length; j++) {

                var value = parserController.properties[i][j].value;

                if(value != '') {
                    parserController.properties[i][j].attach(value);
                }
            }
        }
    },

    // clean: function() {
    // 	// call execute of each object
    // }
};

parserController.properties = [
    [ // navigation
        {
            value: 'Technology, Media & Telecommunications',
            // value: 'light',
            attach: function(value) {
                $('.sub-navbar .industry').html(value);
            },
        },
    ],
    [ // banner
        {
            value: 'dark',
            // value: 'light',
            attach: function(value) {
                $('.banner').addClass(value);
            },
        },
        {
            value: 'https://www2.deloitte.com/content/dam/html/au/mobile-consumer-survey/2019/img/hero.png',
            // value: 'https://www2.deloitte.com/content/dam/html/au/careers/img/hero.png',


            attach: function(value) {
                https://www2.deloitte.com/content/dam/html/au/careers/img/hero.png
                $('.banner').find('.hero-wrapper img').attr('src', value);
            },
        },
        // {
        //     value: 'https://www2.deloitte.com/content/dam/html/au/mobile-consumer-survey/2019/img/au-mcs-hero-1x1.gif?nc=1',
        //     attach: function(value) {
        //         $('.banner').find('.hero-wrapper img').attr('data', value);
        //     },
        // },
        {
            // value: 'Title ipsum dolor sit amet, consectetur adipiscing elit. Aenean ullamcorper sagittis ante vel auctor.',
            value: 'Mobile Consumer Survey 2019',
            attach: function(value) {
                $('.banner').find('.headline h1').html(value);
            },
        },
        {
            // value: 'Description dolor sit amet, consectetur adipiscing elit. Aenean ullamcorper sagittis ante vel auctor.',
            value: 'Unwired. Unrivalled. Unknown.',
            attach: function(value) {
                $('.banner').find('.subtitle').html(value);
            },
        },
        {
            value: 'https://www2.deloitte.com',
            attach: function(value) {
                $('.banner').find('.headline-cta a').attr('href', value);
            },
        },
        {
            value: 'Download the report',
            attach: function(value) {
                $('.banner').find('.headline-cta a').html(value);
            },
        },
    ],
    [ // about
        {
            value: 'https://www2.deloitte.com/content/dam/html/au/devless/img/au-about-image-placeholder.jpg',
            attach: function(value) {
                $('.about').find('img').attr('src', value);
            },
        },
        {
            value: 'Our social impact matters',
            attach: function(value) {
                $('.about').find('h3').html(value);
            },
        },
        {
            value: '<em>Stories of Impact</em> demonstrates how Deloitte Australia makes a significant and distinctive contribution to our economy and to our nation. \
                    We take seriously our mission to make a positive social impact in Australia and beyond. Today’s world is fast-paced, ever-changing and often unpredictable. \
                    We face many big challenges – including climate change, health, education, skills shortages, poverty, and poor social outcomes affecting our First Peoples. \
                    We believe people increasingly expect business leaders to play their part and influence policymakers in seeking solutions to these vital local, national and global challenges. <br><br>\
                    Deloitte Australia’s depth and range of expertise exists to help drive prosperity for our entire nation. This 2019 Social Impact Report presents leading-edge thinking as well \
                    as evidence-based, design-focused and purpose-led examples of the tangible ways we are turning problems into opportunities for our clients, our people and our communities. ',
            attach: function(value) {
                $('.about').find('.description').html(value);
            },
        },
    ],
    [ // video
        {
            value: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris non convallis ligula, non feugiat justo. ',
            attach: function(value) {
                $('.video').find('h3').html(value);
            },
        },
        {
            value: 'We believe people increasingly expect business leaders to play their part and influence policy- makers in seeking solutions to vital local, national and global challenges.',
            attach: function(value) {
                $('.video').find('p').html(value);
            },
        },
        {
            value: '3591071399001',
            attach: function(value) {
                $('.video').find('.video-account-id').html(value);
            },
        },
        {
            value: 'SJ5dT6Cj',
            attach: function(value) {
                $('.video').find('.video-player-id').html(value);
            },
        },
        {
            value: '6114471767001',
            attach: function(value) {
                $('.video').find('.video-id').html(value);
            },
        },
    ],

    [ // videoplaylist
        {
            value: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris non convallis ligula, non feugiat justo. ',
            attach: function(value) {
                $('.videoplaylist').find('h3').html(value);
            },
        },
        {
            value: 'We believe people increasingly expect business leaders to play their part and influence policy- makers in seeking solutions to vital local, national and global challenges.',
            attach: function(value) {
                $('.videoplaylist').find('p').html(value);
            },
        },
        {
            value: '3591071399001',
            attach: function(value) {
                $('.videoplaylist').find('.video-account-id').html(value);
            },
        },
        {
            value: 'S1QEA6Co',
            attach: function(value) {
                $('.videoplaylist').find('.video-player-id').html(value);
            },
        },
        {
            value: '5836661128001',
            attach: function(value) {
                $('.videoplaylist').find('.video-playlist-id').html(value);
            },
        },
    ],

    [ // tile
        {
            value: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris non convallis ligula, non feugiat justo. ',
            attach: function(value) {
                $('.tile .section-headline').find('h3').html(value);
            },
        },
        {
            value: 'We believe people increasingly expect business leaders to play their part and influence policy- makers in seeking solutions to vital local, national and global challenges.',
            attach: function(value) {
                $('.tile .section-headline').find('p').html(value);
            },
        },
        {
            value: 'https://www2.deloitte.com/content/dam/insights/us/articles/4700_Economics-weekly_June18/images/4700_1440x660.jpg/jcr:content/renditions/cq5dam.web.480.300.jpeg',
            attach: function(value) {
                $('.tile-cta').eq(0).find('img').attr('src', value);
                $('.tile-cta').eq(0).find('.title').html('1 Music, video and');
                $('.tile-cta').eq(0).find('.type').html('Blog');
                //$('.tile-cta').eq(0).attr('href', 'https://www2.deloitte.com');

                var popupContent = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ac molestie lectus, vitae porta ante. Pellentesque at elementum turpis. Proin sed ornare. <br>\
                                    <ul>\
                                    <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ac molestie lectus, vitae porta ante. Pellentesque at elementum turpis. Proin sed ornare ipsum. Fusce facilisis urna aliquet libero suscipit pellentesque. Nulla sagittis diam leo. Nulla dignissim arcu ac tincidunt luctus. Proin nulla turpis, feugiat at imperdiet tempus, viverra ac mauris.</li>\
                                    <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ac molestie lectus, vitae porta ante. Pellentesque at elementum turpis. Proin sed ornare ipsum.</li>\
                                    <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ac molestie lectus, vitae porta ante.</li>\
                                    </ul><br>\
                                    <ol>\
                                    <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ac molestie lectus, vitae porta ante. Pellentesque at elementum turpis. Proin sed ornare ipsum. Fusce facilisis urna aliquet libero suscipit pellentesque. Nulla sagittis diam leo. Nulla dignissim arcu ac tincidunt luctus. Proin nulla turpis, feugiat at imperdiet tempus, viverra ac mauris.</li>\
                                    <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ac molestie lectus, vitae porta ante. Pellentesque at elementum turpis. Proin sed ornare ipsum.</li>\
                                    <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ac molestie lectus, vitae porta ante.</li>\
                                    </ol><br>\
                                    Download the report <a href="#" target="_blank">here</a>.';
                $('.tile-cta').eq(0).parent().find('.popup-content').html(popupContent);
                
            },
        },
        {
            value: 'https://www2.deloitte.com/content/dam/insights/us/articles/6354_Cocreation-for-impact/images/6354_banner.jpg/jcr:content/renditions/cq5dam.web.480.300.jpeg',
            attach: function(value) {
                $('.tile-cta').eq(1).find('img').attr('src', value);
                $('.tile-cta').eq(1).find('.title').html('2 Music, video and gaming remain domain Music, video and gaming');
                $('.tile-cta').eq(1).find('.type').html('Blog');
                $('.tile-cta').eq(1).attr('href', 'https://www2.deloitte.com');
            },
        },
        {
            value: 'https://www2.deloitte.com/content/dam/insights/us/articles/GLOB43116/images/GLOB43116_banner.png/jcr:content/renditions/cq5dam.web.480.300.jpeg',
            attach: function(value) {
                $('.tile-cta').eq(2).find('img').attr('src', value);
                $('.tile-cta').eq(2).find('.title').html('3 Music, video and gaming remain domain');
                $('.tile-cta').eq(2).find('.type').html('Blog');
                $('.tile-cta').eq(2).attr('href', 'https://www2.deloitte.com');
            },
        },
        {
            value: 'https://www2.deloitte.com/content/dam/insights/us/articles/6358_2019-global-blockchain-study/images/6358_banner.jpg/jcr:content/renditions/cq5dam.web.480.300.jpeg',
            attach: function(value) {
                $('.tile-cta').eq(3).find('img').attr('src', value);
                $('.tile-cta').eq(3).find('.title').html('4 Music, video and gaming remain domain');
                $('.tile-cta').eq(3).find('.type').html('Blog');
                $('.tile-cta').eq(3).attr('href', 'https://www2.deloitte.com');
            },
        },
        {
            value: 'https://www2.deloitte.com/content/dam/insights/us/articles/6404-future-of-healthcare/images/6404_banner_correct-size.gif/jcr:content/renditions/cq5dam.web.480.300.jpeg',
            attach: function(value) {
                $('.tile-cta').eq(4).find('img').attr('src', value);
                $('.tile-cta').eq(4).find('.title').html('5 Music, video and gaming remain domain');
                $('.tile-cta').eq(4).find('.type').html('Blog');
                $('.tile-cta').eq(4).attr('href', 'https://www2.deloitte.com');
            },
        },
        {
            value: 'https://www2.deloitte.com/content/dam/insights/us/articles/4983-tech-trends-19-video-image/images/4983_video_1440x660_banner.jpg/jcr:content/renditions/cq5dam.web.480.300.jpeg',
            attach: function(value) {
                $('.tile-cta').eq(5).find('img').attr('src', value);
                $('.tile-cta').eq(5).find('.title').html('6 Music, video and gaming remain domain');
                $('.tile-cta').eq(5).find('.type').html('Blog');
                $('.tile-cta').eq(5).attr('href', 'https://www2.deloitte.com');
            },
        },
        {
            value: 'https://www2.deloitte.com/content/dam/insights/us/articles/4354_Real-World-Evidence/images/4354_RWE-benchmarking-survey_1440x660.jpg/jcr:content/renditions/cq5dam.web.480.300.jpeg',
            attach: function(value) {
                $('.tile-cta').eq(6).find('img').attr('src', value);
                $('.tile-cta').eq(6).find('.title').html('7 Music, video and gaming remain domain');
                $('.tile-cta').eq(6).find('.type').html('Blog');
                $('.tile-cta').eq(6).attr('href', 'https://www2.deloitte.com');
            },
        },
        {
            value: 'https://www2.deloitte.com/content/dam/insights/us/multimedia/navigating-future-of-work-video/images/4062_promo-1440x660.jpg/jcr:content/renditions/cq5dam.web.480.300.jpeg',
            attach: function(value) {
                $('.tile-cta').eq(7).find('img').attr('src', value);
                $('.tile-cta').eq(7).find('.title').html('8 Music, video and gaming remain domain');
                $('.tile-cta').eq(7).find('.type').html('Blog');
                $('.tile-cta').eq(7).attr('href', 'https://www2.deloitte.com');
            },
        },
        {
            value: 'https://www2.deloitte.com/content/dam/insights/us/articles/GLOB43116/images/GLOB43116_banner.png/jcr:content/renditions/cq5dam.web.480.300.jpeg',
            attach: function(value) {
                $('.tile-cta').eq(8).find('img').attr('src', value);
                $('.tile-cta').eq(8).find('.title').html('9 Music, video and gaming remain domain');
                $('.tile-cta').eq(8).find('.type').html('Blog');
                $('.tile-cta').eq(8).attr('href', 'https://www2.deloitte.com');
            },
        },
        {
            value: 'https://www2.deloitte.com/content/dam/insights/us/articles/6358_2019-global-blockchain-study/images/6358_banner.jpg/jcr:content/renditions/cq5dam.web.480.300.jpeg',
            attach: function(value) {
                $('.tile-cta').eq(9).find('img').attr('src', value);
                $('.tile-cta').eq(9).find('.title').html('10 Music, video and gaming remain domain');
                $('.tile-cta').eq(9).find('.type').html('Blog');
                $('.tile-cta').eq(9).attr('href', 'https://www2.deloitte.com');
            },
        },
        {
            value: 'https://www2.deloitte.com/content/dam/insights/us/articles/6404-future-of-healthcare/images/6404_banner_correct-size.gif/jcr:content/renditions/cq5dam.web.480.300.jpeg',
            attach: function(value) {
                $('.tile-cta').eq(10).find('img').attr('src', value);
                $('.tile-cta').eq(10).find('.title').html('11 Music, video and gaming remain domain');
                $('.tile-cta').eq(10).find('.type').html('Blog');
                $('.tile-cta').eq(10).attr('href', 'https://www2.deloitte.com');
            },
        },
        {
            value: 'https://www2.deloitte.com/content/dam/insights/us/articles/4983-tech-trends-19-video-image/images/4983_video_1440x660_banner.jpg/jcr:content/renditions/cq5dam.web.480.300.jpeg',
            attach: function(value) {
                $('.tile-cta').eq(11).find('img').attr('src', value);
                $('.tile-cta').eq(11).find('.title').html('12 Music, video and gaming remain domain');
                $('.tile-cta').eq(11).find('.type').html('Blog');
                $('.tile-cta').eq(11).attr('href', 'https://www2.deloitte.com');
            },
        },
    ],

    [ // imageinfographic
        {
            value: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris non convallis ligula, non feugiat justo. ',
            attach: function(value) {
                $('.imageinfographic').find('h3').html(value);
            },
        },
        {
            value: 'We believe people increasingly expect business leaders to play their part and influence policy- makers in seeking solutions to vital local, national and global challenges.',
            attach: function(value) {
                $('.imageinfographic').find('p').html(value);
            },
        },
        {
            value: 'https://www2.deloitte.com/content/dam/Deloitte/au/Images/infographics/au-tax-federal-budget-2019-030419.jpg',
            // value: 'http://placeimg.com/1200/500/animals',
            attach: function(value) {
                $('.imageinfographic').find('img').attr('src', value);
            },
        },
    ],
    [ // related
        {
            value: 'Related Articles',
            attach: function(value) {
                $('.related').find('h3').html(value);
            },
        },
        {
            value: 'We believe people increasingly expect business leaders to play their part and influence policy- makers in seeking solutions to vital local, national and global challenges.',
            attach: function(value) {
                $('.related').find('p').html(value);
            },
        },
        {
            value: 'https://www2.deloitte.com/content/dam/insights/us/articles/4983-tech-trends-19-video-image/images/4983_video_1440x660_banner.jpg/jcr:content/renditions/cq5dam.web.480.300.jpeg',
            attach: function(value) {
                $('.related-block').eq(0).find('img').attr('src', value);
                $('.related-block').eq(0).find('.title').html('How the financial crisis reshaped the world’s workforce');
                $('.related-block').eq(0).attr('href', 'https://www2.deloitte.com');
            },
        },
        {
            value: 'https://www2.deloitte.com/content/dam/insights/us/multimedia/navigating-future-of-work-video/images/4062_promo-1440x660.jpg/jcr:content/renditions/cq5dam.web.480.300.jpeg',
            attach: function(value) {
                $('.related-block').eq(1).find('img').attr('src', value);
                $('.related-block').eq(1).find('.title').html('Picturing how advanced technologies are reshaping mobility');
                $('.related-block').eq(1).attr('href', 'https://www2.deloitte.com');
            },
        },
        {
            value: 'https://www2.deloitte.com/content/dam/insights/us/articles/6358_2019-global-blockchain-study/images/6358_banner.jpg/jcr:content/renditions/cq5dam.web.480.300.jpeg',
            attach: function(value) {
                $('.related-block').eq(2).find('img').attr('src', value);
                $('.related-block').eq(2).find('.title').html('Engaging employees like consumers');
                $('.related-block').eq(2).attr('href', 'https://www2.deloitte.com');
            },
        },
        {
            value: 'https://www2.deloitte.com/content/dam/insights/us/articles/GLOB43116/images/GLOB43116_banner.png/jcr:content/renditions/cq5dam.web.480.300.jpeg',
            attach: function(value) {
                $('.related-block').eq(3).find('img').attr('src', value);
                $('.related-block').eq(3).find('.title').html('How leaders are navigating the Fourth Industrial Revolution');
                $('.related-block').eq(3).attr('href', 'https://www2.deloitte.com');
            },
        },
        {
            value: 'https://www2.deloitte.com/content/dam/insights/us/articles/4983-tech-trends-19-video-image/images/4983_video_1440x660_banner.jpg/jcr:content/renditions/cq5dam.web.480.300.jpeg',
            attach: function(value) {
                $('.related-block').eq(4).find('img').attr('src', value);
                $('.related-block').eq(4).find('.title').html('How the financial crisis reshaped');
                $('.related-block').eq(4).attr('href', 'https://www2.deloitte.com');
            },
        },
        {
            value: 'https://www2.deloitte.com/content/dam/insights/us/multimedia/navigating-future-of-work-video/images/4062_promo-1440x660.jpg/jcr:content/renditions/cq5dam.web.480.300.jpeg',
            attach: function(value) {
                $('.related-block').eq(5).find('img').attr('src', value);
                $('.related-block').eq(5).find('.title').html('Picturing how advanced technologies');
                $('.related-block').eq(5).attr('href', 'https://www2.deloitte.com');
            },
        },
        {
            value: 'https://www2.deloitte.com/content/dam/insights/us/articles/6358_2019-global-blockchain-study/images/6358_banner.jpg/jcr:content/renditions/cq5dam.web.480.300.jpeg',
            attach: function(value) {
                $('.related-block').eq(6).find('img').attr('src', value);
                $('.related-block').eq(6).find('.title').html('Engaging employees like consumers');
                $('.related-block').eq(6).attr('href', 'https://www2.deloitte.com');
            },
        },
        {
            value: 'https://www2.deloitte.com/content/dam/insights/us/articles/GLOB43116/images/GLOB43116_banner.png/jcr:content/renditions/cq5dam.web.480.300.jpeg',
            attach: function(value) {
                $('.related-block').eq(7).find('img').attr('src', value);
                $('.related-block').eq(7).find('.title').html('How leaders are navigating ');
                $('.related-block').eq(7).attr('href', 'https://www2.deloitte.com');
            },
        },
    ],
    [ // contact
        {
            value: 'Know more from our leaders',
            attach: function(value) {
                $('.contact').find('h3').html(value);
            },
        },
        {
            value: 'We believe people increasingly expect business leaders to play their part and influence policy- makers in seeking solutions to vital local, national and global challenges.',
            attach: function(value) {
                $('.contact .section-headline').find('p').html(value);
            },
        },
        {
            value: 'https://www2.deloitte.com/content/dam/Deloitte/au/Images/promo_images/au-profiles/au-matt-mcgrath-1x1.jpg/_jcr_content/renditions/cq5dam.web.231.231.desktop.jpeg',
            attach: function(value) {
                $('.contact-block').eq(0).find('img').attr('src', value);
                $('.contact-block').eq(0).find('.name').html('Matt McGrath');
                $('.contact-block').eq(0).find('.name').attr('href', 'https://www2.deloitte.com/au/en/profiles/matt-mcgrath.html');
                $('.contact-block').eq(0).find('.position').html('Chief Marketing Officer, Deloitte Australia, Chief Marketing Officer, Deloitte Australia');
                $('.contact-block').eq(0).find('.email').html('mamcgrath@deloitte.com.au');
            },
        },
        {
            value: 'https://www2.deloitte.com/content/dam/Deloitte/au/Images/promo_images/au-profiles/au-tony-odonnell-1x1.jpg/_jcr_content/renditions/cq5dam.web.231.231.desktop.jpeg',
            attach: function(value) {
                $('.contact-block').eq(1).find('img').attr('src', value);
                $('.contact-block').eq(1).find('.name').html('Tony O’Donnell');
                $('.contact-block').eq(1).find('.name').attr('href', 'https://www2.deloitte.com/au/en/profiles/tony-odonnell.html');
                $('.contact-block').eq(1).find('.position').html('Lead Partner, Strategic Cost Transformation');
                $('.contact-block').eq(1).find('.email').html('tonyodonnell@deloitte.com.au');
            },
        },
        {
            value: 'https://www2.deloitte.com/content/dam/Deloitte/au/Images/promo_images/au-profiles/au-anna-clive-2019-1x1.jpg/_jcr_content/renditions/cq5dam.web.231.231.desktop.jpeg',
            attach: function(value) {
                $('.contact-block').eq(2).find('img').attr('src', value);
                $('.contact-block').eq(2).find('.name').attr('href', 'https://www2.deloitte.com/au/en/profiles/anna-clive.html');
                $('.contact-block').eq(2).find('.name').html('Anna Clive');
                $('.contact-block').eq(2).find('.position').html('Director, Consulting');
                $('.contact-block').eq(2).find('.email').html('aclive@deloitte.com.au');
            },
        },
        {
            value: 'https://www2.deloitte.com/content/dam/Deloitte/au/Images/promo_images/au-profiles/au-gerry-wilde-1x1.jpg',
            attach: function(value) {
                $('.contact-block').eq(3).find('img').attr('src', value);
                $('.contact-block').eq(3).find('.name').html('Kevin Russo');
                $('.contact-block').eq(3).find('.name').attr('href', 'https://www2.deloitte.com/au/en/profiles/kevin-russo.html');
                $('.contact-block').eq(3).find('.position').html('Partner, Consulting');
                $('.contact-block').eq(3).find('.email').html('krusso@deloitte.com.au');
            },
        },
        {
            value: 'https://www2.deloitte.com/content/dam/Deloitte/au/Images/promo_images/au-profiles/au-anna-clive-2019-1x1.jpg/_jcr_content/renditions/cq5dam.web.231.231.desktop.jpeg',
            attach: function(value) {
                $('.contact-block').eq(4).find('img').attr('src', value);
                $('.contact-block').eq(4).find('.name').attr('href', 'https://www2.deloitte.com/au/en/profiles/anna-clive.html');
                $('.contact-block').eq(4).find('.name').html('Anna Clive');
                $('.contact-block').eq(4).find('.position').html('Director, Consulting');
                $('.contact-block').eq(4).find('.email').html('aclive@deloitte.com.au');
            },
        },
        {
            value: 'https://www2.deloitte.com/content/dam/Deloitte/au/Images/promo_images/au-profiles/au-gerry-wilde-1x1.jpg',
            attach: function(value) {
                $('.contact-block').eq(5).find('img').attr('src', value);
                $('.contact-block').eq(5).find('.name').html('Kevin Russo');
                $('.contact-block').eq(5).find('.name').attr('href', 'https://www2.deloitte.com/au/en/profiles/kevin-russo.html');
                $('.contact-block').eq(5).find('.position').html('Partner, Consulting');
                $('.contact-block').eq(5).find('.email').html('krusso@deloitte.com.au');
            },
        },
    ],
    [ // image carousel
        {
            value: 'Carousel headline',
            attach: function(value) {
                $('.imagecarousel').find('h3').html(value);
            },
        },
        {
            value: 'We believe people increasingly expect business leaders to play their part and influence policy- makers in seeking solutions to vital local, national and global challenges.',
            attach: function(value) {
                $('.imagecarousel .section-headline').find('p').html(value);
            },
        },
        {
            value: 'http://placeimg.com/1000/500/animals',
            attach: function(value) {
                $('.imagecarousel .imagecarousel-data').eq(0).attr('src', 'https://www2.deloitte.com/content/dam/html/au/mobile-consumer-survey/2019/img/au-mcs-carousel1-2x1.jpg');
                $('.imagecarousel .imagecarousel-data').eq(1).attr('src', 'https://www2.deloitte.com/content/dam/html/au/mobile-consumer-survey/2019/img/au-mcs-carousel2-2x1.jpg');
                $('.imagecarousel .imagecarousel-data').eq(2).attr('src', 'https://www2.deloitte.com/content/dam/html/au/mobile-consumer-survey/2019/img/au-mcs-carousel3-2x1.jpg');
                $('.imagecarousel .imagecarousel-data').eq(3).attr('src', 'https://www2.deloitte.com/content/dam/html/au/mobile-consumer-survey/2019/img/au-mcs-carousel4-2x1.jpg');
                $('.imagecarousel .imagecarousel-data').eq(4).attr('src', 'https://www2.deloitte.com/content/dam/html/au/mobile-consumer-survey/2019/img/au-mcs-carousel5-2x1.jpg');
                $('.imagecarousel .imagecarousel-data').eq(5).attr('src', 'https://www2.deloitte.com/content/dam/html/au/mobile-consumer-survey/2019/img/au-mcs-carousel6-2x1.jpg');
            },
        },
    ],
    [ // image carousel
        {
            value: 'Twine headline',
            attach: function(value) {
                $('.twine').find('h3').html(value);
            },
        },
        {
            value: 'We believe people increasingly expect business leaders to play their part and influence policy- makers in seeking solutions to vital local, national and global challenges.',
            attach: function(value) {
                $('.twine .section-headline').find('p').html(value);
            },
        },
        {
            value: '//apps.twinesocial.com/embed?app=purpose&showNav=yes&showLoadMore=no&autoload=no',
            attach: function(value) {
                $('.twine .twine-drop').html('//apps.twinesocial.com/embed?app=purpose&showNav=yes&showLoadMore=no&autoload=no');
            },
        },
    ],
];