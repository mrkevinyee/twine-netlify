var appController = {
    modal: null,
    rtime: null,
    timeout: !1,
    delta: 200,
    toolbarOptions: null,
    quillModalEditor: null,
    delimiter: '--',
    startTable: '[start-table]',
    endTable: '[end-table]',

    init: function() {
        modalController.init(),
        appController.start()
    },

    start: function() {
        appController.initVariables(),
        appController.initEvents(),
        appController.updateText(),
        appController.setColumnDropdown()
    },

    initEvents: function() {
        document.getElementById("convert").onclick = function(t) {
            t.preventDefault();
            $("#result").text(appController.convertText());
        },

        document.getElementById("import").onclick = function(t) {
            t.preventDefault();
            appController.importHTML();
        },

        document.getElementById("preview").onclick = function(t) {
            t.preventDefault();
            $("#convert").click();
            var e = $("#result").contents()[0].textContent;
            appController.previewModal(e);
        },

        document.getElementById("copy-text").onclick = function(t) {
            appController.copyToClipboard($('#result').text());
            alert('HTML copied to clipboard.');
        }
    },

    initVariables: function() {
        var icons = Quill.import('ui/icons');
        icons['code-block'] = '<svg viewBox="0 0 200 200" xmlns="http://www.w3.org/2000/svg"><image href="https://www2.deloitte.com/content/dam/html/global/scp/modal-editor/table_icon.svg" height="200" width="200"/></svg>';

        appController.toolbarOptions = [
            ["bold", "italic"], ["link"], [{
            script: "sub"
        }, {
            script: "super"
        }], [{
            list: "ordered"
        }, {
            list: "bullet"
        }], ["clean"], ["image"], ["code-block"],
        [{ 'columns': ['2', '3', '4'] }],
        ],
        appController.quillModalEditor = new Quill("#editor",{
            theme: "snow",
            placeholder: "Enter text",
            formats: ["bold", "italic", "script", "image", "link", "list", "indent"],
            modules: {
                toolbar: {
                    container: appController.toolbarOptions,
                    handlers: {
                        image: appController.imageHandler,
                        'code-block': appController.tableHandler,
                        'columns': function (value) { 
                            if (value) {
                                appController.setColumnDropdown(value);
                            }
                        }
                    }
                }
            }
        })
    },

    imageHandler: function() {
        var quill = appController.quillModalEditor;
        var range = quill.getSelection();
        var value = prompt('What is the image URL?');

        if(value){
            quill.insertEmbed(range.index, 'image', value, Quill.sources.USER);
        }
    },

    tableHandler: function() {
        var quill = appController.quillModalEditor;
        var range = quill.getSelection();
        quill.insertText(range.index, appController.startTable + '\n\n' + appController.endTable + '\n');
    },

    convertText: function() {
        var html = [];
        var columnCount = $('.ql-columns .ql-picker-label').attr('data-value');
        var result = "";
        var inTable = false;

        // Seperate each line
        $('#editor').children().first().children().each(function () {
            html.push(this);
        });
        
        // Set up seperators
        var fullWidthStart =
        '<div class="row"\>' +
        '\n    <div class="col-md-12"\>';

        var fullWidthEnd =
        '\n    </div>' +
        '\n</div>\n';

        var start = 
        '<div class="row"\>' +
        '\n    <div class="col-md-12"\>' +
        '\n        <div class="flex-container"\>' + 
        '\n            <div class="flex-column by-' + columnCount + '">' +
        '\n                 ';

        var end = 
        '\n            </div>' +
        '\n        </div>' +
        '\n    </div>' +
        '\n</div>\n';
    
        var seperators = 
        '\n            </div>' +
        '\n            <div class="flex-column by-'+ columnCount +'">' +
        '\n                ';

        // Add seperators
        for (var count = 0; count < html.length; count++) {
            var currLine = html[count];

            if (currLine.textContent.trim() == appController.startTable) {
                result += start;
                inTable = true;

            } else if (currLine.textContent.trim() == appController.endTable) {
                result += end;
                inTable = false;

            } else if (currLine.textContent.trim() == appController.delimiter) {
                result += seperators;

            } else {
                if (inTable) {
                    result += currLine.outerHTML;
                } else {
                    var nextLine = (html[count + 1] && html[count + 1].textContent.trim() == appController.startTable) || count == html.length - 1;
                    var prevLine = (html[count - 1] && html[count - 1].textContent.trim() == appController.endTable) || count == 0;

                    if (nextLine && prevLine) {
                        result += fullWidthStart;
                        result += '\n        ' + currLine.outerHTML;
                        result += fullWidthEnd;

                    // If table is starting next line or final line
                    } else if (nextLine) {
                        result += currLine.outerHTML;
                        result += fullWidthEnd;
                    
                    // If table ended previous line or first line
                    } else if (prevLine) {
                        result += fullWidthStart;
                        result += '\n        ' + currLine.outerHTML;

                    } else {
                        result += currLine.outerHTML;
                    }
                }
            }
        }

        return result;
    },

    importHTML: function() {
        var text = document.getElementById("import-text-editor").value.trim();
        var html = $.parseHTML(text);
        var output = '';

        // Find column count
        var columns = $(html).find('.flex-column');

        if (columns.length > 0) {
            var columnCount = columns.first().attr('class').split(' ')[1].match(/[0-9]/);
        } else {
            var columnCount = 1;
        }

        $(html).children().each(function (index) {
            // Remove non html
            if (!$(this).prop("tagName")) {
                return;
            }

            // Check if inside a table
            if ($(this).has('div').length == 0) {
                output += $(this).html().trim();
            } else {
                output += appController.startTable;

                if (columns.length > 0) {
                    $(this).find('.flex-column').each(function (index) {
                        output += $(this).html().trim();
                        output += appController.delimiter;
                    });
        
                } else {
                    $(this).first().children().each(function (index) {
                        output += $(this).html().trim();
                        output += appController.delimiter;
                    });
                }
                output += appController.endTable;
            }
        });    

        output = output.replace(/--\[end-table\]/g, appController.endTable);
        appController.quillModalEditor.root.innerHTML = output;
        appController.setColumnDropdown(columnCount);
    },

    previewModal: function(text) {
        var data = {
            title: '**Title here is auto-generated from tile title**',
            popupContent: text,
        };

        modalController.openModal(data);
    },
    
    copyToClipboard: function (str) {
        var element = document.createElement('textarea');
        element.value = str;
        element.setAttribute('readonly', '');
        element.style.position = 'absolute';
        element.style.left = '-9999px';
        document.body.appendChild(element);

        var selected = document.getSelection().rangeCount > 0
            ? document.getSelection().getRangeAt(0)
            : false;
    
        element.select();
        document.execCommand('copy');
        document.body.removeChild(element);

        if (selected) {
            document.getSelection().removeAllRanges();
            document.getSelection().addRange(selected);
        }
    },

    updateText: function () {
        $('label[for=editor]').html('Click the table icon to generate a new table<br>Add ' + appController.delimiter + ' on a new line inside a table to seperate sections');
    },

    setColumnDropdown: function (value) {
        if (value) {
            var text = value;
            $('.ql-columns .ql-picker-label').attr('data-value', text);
        } else {
            var text = $('.ql-columns .ql-picker-label').attr('data-value');
        }

        var dropdown = '<svg viewBox="0 0 18 18"> <polygon class="ql-stroke" points="7 11 9 13 11 11 7 11"></polygon> <polygon class="ql-stroke" points="7 7 9 5 11 7 7 7"></polygon> </svg>'

        $.each($('.ql-columns .ql-picker-item'), function(_, node) {
            node.textContent = node.dataset.value;
        });

        $('.ql-columns .ql-picker-label').html(text + ' cols' + dropdown);
    }
};

appController.init();